﻿using System;
using System.Data.OleDb;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace app
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnSubmitLogin_Click(object sender, EventArgs e)
        {
            object result = null;
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + AppDomain.CurrentDomain.GetData("dbFile").ToString();
            string query = "select priv from users where username = ? and password_hash = ?";

            OleDbConnection connection = new OleDbConnection(@connectionString);
            OleDbCommand cmd = new OleDbCommand(query, connection);

            cmd.Parameters.AddWithValue("@p1", txtUser.Text);
            cmd.Parameters.AddWithValue("@p2", sha256(txtPass.Text));
            
            try
            {
                connection.Open();
                result = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("DB ERROR: " + ex);
            }
            finally
            {
                connection.Close();
            }
            
            if (result != null)
            {
                this.Hide();
                CatalogForm catalogForm = new CatalogForm((int)result);
                catalogForm.Show();
            } else
                MessageBox.Show("Incorrect username or password!");
        }

        static string sha256(string password)
        {
            SHA256Managed crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(password), 0, Encoding.ASCII.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }
    }
}
